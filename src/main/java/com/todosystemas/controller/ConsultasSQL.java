/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.todosystemas.controller;

import com.todosystemas.dbconfig.dbconfig;
import com.todosystemas.model.ResActividad;
import com.todosystemas.model.actividad;
import com.todosystemas.model.empleado;
import com.todosystemas.model.requestactividad;
import com.todosystemas.model.tarea;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yonatan
 */
public class ConsultasSQL extends dbconfig {

    public List<empleado> lstEmpleados() {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("select se.sys_num_doc, se.sys_tipo_doc,se.sys_nom_empleado,sd.sys_dept ,se.sys_estado  \n");
            sql.append("from sys_empleado se \n");
            sql.append("right join sys_departamento sd on se.id_depmt = sd.id \n");
            sql.append("where se.sys_estado = 1 ");
            List<empleado> lstEmpleados = new ArrayList<>();
            try (PreparedStatement ps = mysqlcon().prepareStatement(sql.toString())) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        empleado e = new empleado();
                        e.setNum_doc(rs.getString("sys_num_doc"));
                        e.setTipo_doc(rs.getString("sys_tipo_doc"));
                        e.setNom_empleado(rs.getString("sys_nom_empleado"));
                        e.setDepartamento(rs.getString("sys_dept"));
                        e.setSys_estado(rs.getInt("sys_estado"));
                        lstEmpleados.add(e);
                    }
                }
            }

            return lstEmpleados;

        } catch (Exception e) {
            Logger.getLogger(ConsultasSQL.class.getName()).log(Level.SEVERE, e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<tarea> lstTareas() {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("select st.id ,sd.sys_dept , st.sys_sla_horas ,st.sys_sla_dias ,st.sys_tarea , st.estado \n");
            sql.append("from sys_tarea st  \n");
            sql.append("right join sys_departamento sd on st.id_deptm = sd.id  \n");
            sql.append("where st.estado = 1");
            List<tarea> lsttareas = new ArrayList<>();
            try (PreparedStatement ps = mysqlcon().prepareStatement(sql.toString())) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        tarea t = new tarea();
                        t.setDepartamento(rs.getString("sys_dept"));
                        t.setEstado(rs.getInt("estado"));
                        t.setId(rs.getInt("id"));
                        t.setSys_sla_dias(rs.getInt("sys_sla_dias"));
                        t.setSys_sla_horas(rs.getInt("sys_sla_horas"));
                        t.setSys_tarea(rs.getString("sys_tarea"));
                        lsttareas.add(t);
                    }
                }
            }

            return lsttareas;

        } catch (Exception e) {
            Logger.getLogger(ConsultasSQL.class.getName()).log(Level.SEVERE, e.getMessage());
            return new ArrayList<>();
        }
    }

    public tarea getTarea(int id) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT st.id, st.id_deptm, st.sys_sla_horas, st.sys_sla_dias, st.sys_tarea, st.estado \n"
                    + "FROM todosistemas.sys_tarea st "
                    + "WHERE st.id = ?");
            tarea t = new tarea();
            try (PreparedStatement ps = mysqlcon().prepareStatement(sql.toString())) {
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        t.setEstado(rs.getInt("estado"));
                        t.setId(rs.getInt("id"));
                        t.setSys_sla_dias(rs.getInt("sys_sla_dias"));
                        t.setSys_sla_horas(rs.getInt("sys_sla_horas"));
                        t.setSys_tarea(rs.getString("sys_tarea"));
                    }
                }
            }

            return t;

        } catch (Exception e) {
            Logger.getLogger(ConsultasSQL.class.getName()).log(Level.SEVERE, e.getMessage());
            return new tarea();
        }
    }

    public List<ResActividad> lstActividaes() {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("select sa.id,sa.actividad ,sa.fecha_ini ,sa.fecha_fin ,sa.estado_actividad, st.sys_tarea , se.sys_nom_empleado, sd.sys_dept \n");
            sql.append("from sys_actividades sa \n");
            sql.append("inner join sys_tarea st on sa.id_sys_tarea = st.id \n");
            sql.append("inner join sys_empleado se on sa.id_sys_empleado = se.sys_num_doc \n");
            sql.append("inner join sys_departamento sd on st.id_deptm = sd.id \n");
            sql.append("where sa.estado =1");
            List<ResActividad> lstActividades = new ArrayList<>();
            try (PreparedStatement ps = mysqlcon().prepareStatement(sql.toString())) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        ResActividad t = new ResActividad();
                        t.setId(rs.getInt("id"));
                        t.setActividad(rs.getString("actividad"));
                        t.setFecha_ini(rs.getTimestamp("fecha_ini"));
                        t.setFecha_fin(rs.getTimestamp("fecha_fin"));
                        t.setEstado_actividad(rs.getInt("estado_actividad"));
                        t.setSys_tarea(rs.getString("sys_tarea"));
                        t.setSys_nombre_empleado(rs.getString("sys_nom_empleado"));
                        t.setSys_dept(rs.getString("sys_dept"));
                        lstActividades.add(t);

                    }
                }
            }

            return lstActividades;

        } catch (Exception e) {
            Logger.getLogger(ConsultasSQL.class.getName()).log(Level.SEVERE, e.getMessage());
            return new ArrayList<>();
        }
    }

    public actividad getActividad(int id) {
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT sa.estado_actividad, sa.estado sa.fecha_fin  \n");
            sql.append("FROM todosistemas.sys_actividades sa ");
            sql.append("WHERE sa.id = ?");
            actividad a = new actividad();
            try (PreparedStatement ps = mysqlcon().prepareStatement(sql.toString())) {
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {

                    while (rs.next()) {
                        a.setEstado(rs.getInt("estado"));
                        a.setEstado_actividad(rs.getInt("estado_actividad"));
                        a.setFecha_fin(rs.getTimestamp("fecha_fin"));

                    }
                }
            }

            return a;

        } catch (Exception e) {
            Logger.getLogger(ConsultasSQL.class.getName()).log(Level.SEVERE, e.getMessage());
            return new actividad();
        }
    }

    public List<actividad> ListActividadUpdate() {
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT sa.id,sa.estado_actividad, sa.estado ,sa.fecha_fin  \n");
            sql.append("FROM todosistemas.sys_actividades sa ");
            sql.append("WHERE sa.estado_actividad = 0 ");
            List<actividad> ListActividadUpdate = new ArrayList<>();
            try (PreparedStatement ps = mysqlcon().prepareStatement(sql.toString())) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        actividad a = new actividad();
                        a.setId(rs.getInt("id"));                       
                        a.setEstado(rs.getInt("estado"));
                        a.setEstado_actividad(rs.getInt("estado_actividad"));
                        a.setFecha_fin(rs.getTimestamp("fecha_fin"));
                        ListActividadUpdate.add(a);

                    }
                }
            }

            return ListActividadUpdate;

        } catch (Exception e) {
            Logger.getLogger(ConsultasSQL.class.getName()).log(Level.SEVERE, e.getMessage());
            return new ArrayList<>();
        }
    }

    public boolean registrarActividad(requestactividad ra) {
        try {
            tarea t = getTarea(ra.getId_tarea());
            Timestamp ts;
            if (t.getSys_sla_horas() > 0) {
                ts = aumentarHoras(t.getSys_sla_horas());
            } else {
                ts = aumentarDias(t.getSys_sla_dias());
            }
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO sys_actividades \n");
            sql.append("(id_sys_tarea, id_sys_empleado , actividad, estado_actividad, estado, fecha_fin) \n");
            sql.append("VALUES (?,?,?,?,?,?)");
            try (PreparedStatement ps = mysqlcon().prepareStatement(sql.toString())) {
                ps.setInt(1, ra.getId_tarea());
                ps.setString(2, ra.getId_empleado());
                ps.setString(3, ra.getActividad());
                ps.setInt(4, 0);
                ps.setInt(5, 1);
                ps.setTimestamp(6, ts);
                if (ps.executeUpdate() == 1) {
                    return true;
                }
            }

            return false;

        } catch (Exception e) {
            Logger.getLogger(ConsultasSQL.class.getName()).log(Level.SEVERE, e.getMessage());
            return false;
        }
    }

    public Timestamp aumentarHoras(int horas) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, horas);
        Timestamp ts = new Timestamp(calendar.getTimeInMillis());
        return ts;
    }

    public Timestamp aumentarDias(int dias) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        Timestamp ts = new Timestamp(calendar.getTimeInMillis());
        return ts;
    }

    public boolean ActualizarEstadoActividad(actividad a, int id) {

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            Timestamp ts = new Timestamp(calendar.getTimeInMillis());
            int estado = 0;
            if (ts.after(a.getFecha_fin())) {
                estado = 2;
            } else {
                estado = 1;
            }

            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE sys_actividades SET estado_actividad=? WHERE id= ?");
            try (PreparedStatement ps = mysqlcon().prepareStatement(sql.toString())) {
                ps.setInt(1, estado);
                ps.setInt(2, id);
                if (ps.executeUpdate() == 1) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            Logger.getLogger(ConsultasSQL.class.getName()).log(Level.SEVERE, e.getMessage());
            return false;
        }

    }

}
