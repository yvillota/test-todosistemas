/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.todosystemas.dbconfig;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yonatan
 */
public class dbconfig {

    
    public Connection mysqlcon() {
        try {
            
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setServerName("localhost");
            dataSource.setPort(3306);
            dataSource.setUser("root");
            dataSource.setPassword("");
            dataSource.setDatabaseName("todosistemas");
            Connection con=dataSource.getConnection();
            return con;

        } catch (SQLException e) {
            Logger.getLogger(dbconfig.class.getName()).log(Level.SEVERE, e.getMessage());
            return null;
        }
    }

    
}
