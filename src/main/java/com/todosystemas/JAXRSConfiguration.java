package com.todosystemas;

import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author Yonatan
 */
@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.todosystemas.CrossOriginResourceSharingFilter.class);
        resources.add(com.todosystemas.resources.JavaEE8Resource.class);
        resources.add(com.todosystemas.resources.RestTodoSystemas.class);
        
    }
    
    
}