/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.todosystemas.resources;

import com.todosystemas.controller.ConsultasSQL;
import com.todosystemas.model.ResActividad;
import com.todosystemas.model.actividad;
import com.todosystemas.model.empleado;
import com.todosystemas.model.requestactividad;
import com.todosystemas.model.tarea;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Yonatan
 */
@Stateless
@LocalBean
@Path("services")
public class RestTodoSystemas {

    @GET
    @Path("empleados")
    @Produces(MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response empleados() {
        try {
            ConsultasSQL s = new ConsultasSQL();
            List<empleado> lstempleados = s.lstEmpleados();
            return Response.status(200).entity(lstempleados).build();
        } catch (Exception e) {
            String respuesta = "{\"mensaje\":\"Error en los datos de consulta\",\"codigo\":2}";
            return Response.serverError().entity(respuesta).build();
        }
    }

    @GET
    @Path("tareas")
    @Produces(MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response tareas() {
        try {
            ConsultasSQL s = new ConsultasSQL();
            List<tarea> lsttareas = s.lstTareas();
            return Response.status(200).entity(lsttareas).build();
        } catch (Exception e) {
            String respuesta = "{\"mensaje\":\"Error en los datos de consulta\",\"codigo\":2}";
            return Response.serverError().entity(respuesta).build();
        }
    }

    @GET
    @Path("act")
    @Produces(MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response actividades() {
        try {
            ConsultasSQL s = new ConsultasSQL();
            List<ResActividad> lstact = s.lstActividaes();
            return Response.status(200).entity(lstact).build();
        } catch (Exception e) {
            String respuesta = "{\"mensaje\":\"Error en los datos de consulta\",\"codigo\":2}";
            return Response.serverError().entity(respuesta).build();
        }
    }

    @POST
    @Path("genact")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response nuevaactividad(requestactividad ra) {
        try {
            ConsultasSQL s = new ConsultasSQL();
            if (s.registrarActividad(ra)) {
                String mensaje = "{\"mensaje\":\"Registro exitoso \",\"codigo\":1}";
                Logger.getLogger(RestTodoSystemas.class.getName()).log(Level.INFO, mensaje);
                return Response.ok().entity(mensaje).build();
            } else {
                String mensaje = "{\"mensaje\":\"Se presento un error al registrar la actividad \",\"codigo\":2}";
                Logger.getLogger(RestTodoSystemas.class.getName()).log(Level.INFO, mensaje);
                return Response.ok().entity(mensaje).build();
            }
        } catch (Exception e) {
            String mensaje = "{\"mensaje\":\"Se presento un error al registrar la actividad \",\"codigo\":2}";
            Logger.getLogger(RestTodoSystemas.class.getName()).log(Level.SEVERE, e.getMessage());
            return Response.serverError().entity(mensaje).build();
        }
    }

    @GET
    @Path("upt/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response nuevaactividad(@PathParam("id") int id) {
        try {
            ConsultasSQL s = new ConsultasSQL();
            actividad a = s.getActividad(id);
            if (a.getFecha_fin() != null) {
                if (s.ActualizarEstadoActividad(a, id)) {
                    String mensaje = "{\"mensaje\":\"Actualización exitosa \",\"codigo\":1}";
                    Logger.getLogger(RestTodoSystemas.class.getName()).log(Level.INFO, mensaje);
                    return Response.ok().entity(mensaje).build();
                } else {
                    String mensaje = "{\"mensaje\":\"Se presento un error al registrar la actividad \",\"codigo\":2}";
                    Logger.getLogger(RestTodoSystemas.class.getName()).log(Level.INFO, mensaje);
                    return Response.ok().entity(mensaje).build();
                }

            } else {
                String mensaje = "{\"mensaje\":\"Se presento un error al registrar la actividad \",\"codigo\":2}";
                Logger.getLogger(RestTodoSystemas.class.getName()).log(Level.INFO, mensaje);
                return Response.ok().entity(mensaje).build();
            }
        } catch (Exception e) {
            String mensaje = "{\"mensaje\":\"Se presento un error al registrar la actividad \",\"codigo\":2}";
            Logger.getLogger(RestTodoSystemas.class.getName()).log(Level.SEVERE, e.getMessage());
            return Response.serverError().entity(mensaje).build();
        }
    }

    @GET
    @Path("uptlist")
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void updatelistaActividad() {
        try {
            ConsultasSQL s = new ConsultasSQL();
            List<actividad> a = s.ListActividadUpdate();

            if (!a.isEmpty()) {
                a.forEach(upt -> {
                    s.ActualizarEstadoActividad(upt, upt.getId());
                });

            }
        } catch (Exception e) {
            Logger.getLogger(RestTodoSystemas.class.getName()).log(Level.SEVERE, e.getMessage());

        }
    }

}
