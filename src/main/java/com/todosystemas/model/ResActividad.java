/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.todosystemas.model;

import java.sql.Timestamp;

/**
 *
 * @author Yonatan
 */
public class ResActividad {

    int id;
    String actividad;
    Timestamp fecha_ini;
    Timestamp fecha_fin;
    int estado_actividad;
    String sys_tarea;
    String sys_nombre_empleado;
    String sys_dept;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public Timestamp getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(Timestamp fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public Timestamp getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Timestamp fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public int getEstado_actividad() {
        return estado_actividad;
    }

    public void setEstado_actividad(int estado_actividad) {
        this.estado_actividad = estado_actividad;
    }

    public String getSys_tarea() {
        return sys_tarea;
    }

    public void setSys_tarea(String sys_tarea) {
        this.sys_tarea = sys_tarea;
    }

    public String getSys_nombre_empleado() {
        return sys_nombre_empleado;
    }

    public void setSys_nombre_empleado(String sys_nombre_empleado) {
        this.sys_nombre_empleado = sys_nombre_empleado;
    }

    public String getSys_dept() {
        return sys_dept;
    }

    public void setSys_dept(String sys_dept) {
        this.sys_dept = sys_dept;
    }

}
