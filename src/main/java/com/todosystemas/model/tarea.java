/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.todosystemas.model;

/**
 *
 * @author Yonatan
 */
public class tarea {

    int id;
    String departamento;
    int sys_sla_horas;
    int sys_sla_dias;
    String sys_tarea;
    int estado;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public int getSys_sla_horas() {
        return sys_sla_horas;
    }

    public void setSys_sla_horas(int sys_sla_horas) {
        this.sys_sla_horas = sys_sla_horas;
    }

    public int getSys_sla_dias() {
        return sys_sla_dias;
    }

    public void setSys_sla_dias(int sys_sla_dias) {
        this.sys_sla_dias = sys_sla_dias;
    }

    public String getSys_tarea() {
        return sys_tarea;
    }

    public void setSys_tarea(String sys_tarea) {
        this.sys_tarea = sys_tarea;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

}
