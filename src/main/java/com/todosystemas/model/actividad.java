/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.todosystemas.model;

import java.sql.Timestamp;

/**
 *
 * @author Yonatan
 */
public class actividad {
    
    int id;
    int id_sys_tarea;
    int id_sys_empleado;
    String info_actividad;
    int estado_actividad;
    int estado;
    Timestamp fecha_ini;
    Timestamp fecha_fin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_sys_tarea() {
        return id_sys_tarea;
    }

    public void setId_sys_tarea(int id_sys_tarea) {
        this.id_sys_tarea = id_sys_tarea;
    }

    public int getId_sys_empleado() {
        return id_sys_empleado;
    }

    public void setId_sys_empleado(int id_sys_empleado) {
        this.id_sys_empleado = id_sys_empleado;
    }

    public String getInfo_actividad() {
        return info_actividad;
    }

    public void setInfo_actividad(String info_actividad) {
        this.info_actividad = info_actividad;
    }

    public int getEstado_actividad() {
        return estado_actividad;
    }

    public void setEstado_actividad(int estado_actividad) {
        this.estado_actividad = estado_actividad;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Timestamp getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(Timestamp fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public Timestamp getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Timestamp fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
    
    
}
