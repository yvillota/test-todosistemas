/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.todosystemas.model;

/**
 *
 * @author Yonatan
 */
public class empleado {

    String num_doc;
    String tipo_doc;
    String nom_empleado;
    String departamento;
    int sys_estado;

    public String getNum_doc() {
        return num_doc;
    }

    public void setNum_doc(String num_doc) {
        this.num_doc = num_doc;
    }

    public String getTipo_doc() {
        return tipo_doc;
    }

    public void setTipo_doc(String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }

    public String getNom_empleado() {
        return nom_empleado;
    }

    public void setNom_empleado(String nom_empleado) {
        this.nom_empleado = nom_empleado;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public int getSys_estado() {
        return sys_estado;
    }

    public void setSys_estado(int sys_estado) {
        this.sys_estado = sys_estado;
    }

}
