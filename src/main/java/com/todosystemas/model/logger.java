/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.todosystemas.model;

import java.sql.Timestamp;

/**
 *
 * @author Yonatan
 */
public class logger {
    int id;
    int id_actividad;
    String log;
    Timestamp date_log;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_actividad() {
        return id_actividad;
    }

    public void setId_actividad(int id_actividad) {
        this.id_actividad = id_actividad;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public Timestamp getDate_log() {
        return date_log;
    }

    public void setDate_log(Timestamp date_log) {
        this.date_log = date_log;
    }
    
    
    
}
